from django.views import generic
from django.shortcuts import render
from django.views.generic import View
from .models import Post
# Create your views here.

class PostView(generic.DetailView):
    model = Post
    template_name = 'post.html'
