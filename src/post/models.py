from django_markdown.models import MarkdownField
from django.db import models
# Create your models here.

class Tag(models.Model):
    slug = models.SlugField(max_length=200,unique=True)

    def __str__(self):
        return self.slug


class PostQuerySet(models.QuerySet):
    def published(self):
        return self.filter(publish=True)

class Post(models.Model):
    title = models.CharField(max_length=100,blank=True,null=True)
    subtitle = models.CharField(max_length=100)
    content = MarkdownField()
    image = models.FileField()
    slug = models.SlugField(max_length=200, unique=True)
    publish = models.BooleanField(default=True)
    created = models.DateField(auto_now_add=True, auto_now=False)
    tags = models.ManyToManyField(Tag)


    objects = PostQuerySet().as_manager()

    def __str__(self):
        return self.title + ' . '

    def get_absolute_url(self):
        return reverse("display", kwargs={"slug": self.slug})

    class Meta:
        verbose_name = "Blog Entry"
        verbose_name_plural = "Blog Entries"
        ordering = ["-created"]
