from django.conf.urls import url
from . import views

app_name = 'post'

urlpatterns = [
#post/pk/name
    url(r'^(?P<slug>\S+)/$',views.PostView.as_view(),name='display'),
]
