from django.shortcuts import render
#generics views
from django.views.generic import View
from django.views import generic
#settings and senmail
from django.conf import settings
from django.core.mail import send_mail
# views and forms
from .forms import  ContactForm
from post import models

# Create your views here.

class IndexView(generic.ListView):
    template_name = 'index.html'
    paginate_by = 5

    def get_queryset(self):
        return models.Post.objects.published()
#def inicio(request):
#	model = models
#	return render(request, "index.html", {})


def contact(request):
	titulo = "Contacto"
	form = ContactForm(request.POST or None)
	if form.is_valid():
#Forma de mostrar decente
#		for key in form.cleaned_data:
#			print(key)
#			print(form.cleaned_data.get(key))
#forma de mostrar a mano
		form_email = form.cleaned_data.get("email")
		form_mensaje = form.cleaned_data.get("mensaje")
		form_nombre = form.cleaned_data.get("nombre")
		asunto = 'Form de contacto'
		email_from = settings.EMAIL_HOST_USER
		email_to = [settings.EMAIL]
		email_mensaje = "%s: %s enviado por : %s" %(form_nombre,form_mensaje,form_email)
		send_mail(asunto,
			email_mensaje,
			email_from,
			email_to,
			fail_silently=False
			)
#		print (email,mensaje,nombre)
	context ={
		"form":form,
		"titulo":titulo,
	}
	return render(request, "contact.html", context)

def about(request):
	return render(request,'about.html',{})
