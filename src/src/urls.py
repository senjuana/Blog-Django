from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
#import de views
from .views import *
#from contacto import views

urlpatterns = [
    #urldefault de la pagina
    url(r'^$',IndexView.as_view(), name='inicio'),
    #subapps
    url(r'^post/',include('post.urls')),
    url(r'^about/$', about, name='about'),
    url(r'^contact/$', contact, name='contact'),
    #admin views
    url(r'^admin/', admin.site.urls),
    #apps de terceros
    url(r'^accounts/', include('registration.backends.default.urls')),
    url('^markdown/', include( 'django_markdown.urls')),
]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
