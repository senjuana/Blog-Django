# Blog Django

![Image](http://i.imgur.com/3uhTBLh.png)
-------------------------------------------------------------------------------------------------------------
![Image](http://i.imgur.com/tx5zhQQ.png)
-------------------------------------------------------------------------------------------------------------
![Image](http://i.imgur.com/TFnmTor.png)
# About
Este Repositorio es un blog Creado en Django con backend,
y el frontend es un proyecto de [bootstrap](https://github.com/BlackrockDigital/startbootstrap-clean-blog)
## Requirements

Supported operating systems:

* Development environments: GNU/Linux, MacOS X.

Requirements:

* virtualenv
* python  >= 3.5
* dajango >= 1.10
* crisy-forms >= 1.6.1
* registration-redux >= 1.4
* django-markdown-app >= 0.8.4
* bootstrap >= 3.3.6

## Getting the code

    $ git clone https://gitlab.com/senjuana/Blog-Django.git
    $ cd pd110


## Runing The code
Una vez dentro de la carpeta delproyecto lo unico que necesitas es hacer correr el servidor de prueba de django

    $ cd bin
    $ source activate && cd ..
    $ cd src  
    $ python manage.py runserver

Una vez el servidor de prueba este corriendo debes de entrar en el puerto local en el cual suele correr django.

http://127.0.0.1:8000/

# Contributing
Si encuentras un bug en el codigo o un issue, por favor notificame de manera privada en
[mi cuenta personal de twitter](https://twitter.com/senjuana).

Este proyecto sigue [code of merit](https://github.com/rosarior/Code-of-Merit). En este repositorio, me importa el codigo,
no opiniones personales o sentimientos. Espero tratar con  adultos.

Antes de enviar un pedazo de codigo por favor verifica que lo que envias funciona, yo no soy la persona que resolvera tus problemas con tu codigo.
